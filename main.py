import spotipy
from spotipy.oauth2 import SpotifyOAuth

from os import getenv
from dotenv import load_dotenv


load_dotenv() # Load client_id, client_secret and redirect_uri
sp = spotipy.Spotify(
    auth_manager=SpotifyOAuth(
        scope="user-library-read",
    )
)

results = sp.current_user_saved_tracks()
for idx, item in enumerate(results["items"]):
    track = item["track"]
    print(idx, track["artists"][0]["name"], " – ", track["name"])
